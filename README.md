## Quickstart

1. Set target hostname in ansible/inventory
2. Edit ansible/mail_stack-config.yaml
3. ```:/$ ansible-playbook role-mail_stack.yaml -i inventory -u root```

## Ansible

ansible 2.x needed on master

### Roles

* mysql
* postfix
* letsencrypt
* dovecot
* mail-stack

#### MySQL
Set up a MySQL(MariaDB) database

##### Config
###### mysql_secure_installation
improve MySQL installation security, set a root pass

set variables in 
mysql/vars/main.yaml
```
mysql_secure_installation: True
mysql_root_password: j9mcbq2r8h
```

#### postfix
Set up postfix, a [mail transfer agent](https://en.wikipedia.org/wiki/Message_transfer_agent)
##### Config

## Postfix & dovecot

Playbook built based on :
https://www.digitalocean.com/community/tutorials/how-to-set-up-a-postfix-e-mail-server-with-dovecot

Postfix deals with ??? 

### submission
Allow sending mail for authorized users

